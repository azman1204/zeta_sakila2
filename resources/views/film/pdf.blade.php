<table border='1'>
    <tr>
        <td>Title</td>
        <td>{{$film->title}}</td>
    </tr>
    <tr>
        <td>Description</td>
        <td>{{$film->description}}</td>
    </tr>
    <tr>
        <td>Rental Rate</td>
        <td>{{$film->rental_rate}}</td>
    </tr>
    <tr>
        <td>Rental Duration</td>
        <td>{{$film->rental_duration}}</td>
    </tr>
</table>
