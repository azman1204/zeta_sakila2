@extends('master')
@section('title', 'Film List')
@section('content')

<form action="" method="post" class="col-md-6">
    @csrf
    <div class="mb-3">
        <label class="form-label">Title</label>
        <input type="text" name="title" class="form-control" value="{{ request()->title }}">
    </div>
    <div class="mb-3">
        <label class="form-label">Rental Duration</label>
        {{ Form::select('rental_duration', $arr, request()->rental_duration,
        ['class' => 'form-control']) }}
    </div>
    <div class="mb-3">
        <input type="submit" class="btn btn-primary">
    </div>
</form>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Rental Rate</th>
            <th>Rental Duration</th>
            <th>Action</th>
        </tr>
        <tbody>
            @foreach ($films as $film)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $film->title }}</td>
                <td>{{ $film->description }}</td>
                <td>{{ $film->rental_rate }}</td>
                <td>{{ $film->rental_duration }}</td>
                <td>
                    <button onclick="getFilm({{ $film->film_id }})" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Detail
                    </button>

                    @can('print film')
                        <a target="_window" href="/film/print/{{ $film->film_id }}" class="btn btn-primary">Print</a>
                    @endcan
                </td>
            </tr>
            @endforeach
        </tbody>
    </thead>
</table>
{{ $films->links() }}

<script>
    function getFilm(film_id) {
        //alert(film_id);
        // AJAX function
        $('#film_content').load('/film/detail/'+film_id);
    }
</script>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body" id="film_content">
            ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
</div>

@endsection
