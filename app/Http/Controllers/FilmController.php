<?php

namespace App\Http\Controllers;
use App\Models\Film;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\App;

class FilmController extends Controller
{
    function print($film_id) {
        $pdf = App::make('dompdf.wrapper');
        $film = Film::find($film_id);
        $content = view('film.pdf', compact('film'))->render();
        $pdf->loadHTML($content);
        // $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->stream();
    }

    // list
    function list(Request $req) {
        if ($req->has('title')) {
            // searching
            $query = Film::whereNotNull('film_id');

            if (! empty($req->title)) {
                $query = $query->where('title', 'like', "%{$req->title}%");
            }

            if ($req->rental_duration !== '0') {
                $query = $query->where('rental_duration', $req->rental_duration);
            }
            $films = $query->paginate(20);
        } else {
            // click from menu
            $films = Film::paginate(20);
        }

        $no = $films->firstItem();
        $arr = [
            '0' => '-- Please Choose --',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
        ];
        return view('film/list', compact('films', 'no', 'arr'));
    }

    // create

    // save

    // edit

    // delete

    // detail
    function detail($film_id) {
        $film = Film::find($film_id);
        return "
            <table class='table table-bordered table-striped'>
                <tr>
                    <td>Title</td>
                    <td>{$film->title}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>{$film->description}</td>
                </tr>
                <tr>
                    <td>Rental Rate</td>
                    <td>{$film->rental_rate}</td>
                </tr>
                <tr>
                    <td>Rental Duration</td>
                    <td>{$film->rental_duration}</td>
                </tr>
            </table>
        ";
    }


}
