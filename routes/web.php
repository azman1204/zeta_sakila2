<?php
use App\Http\Controllers\FilmController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

Route::controller(FilmController::class)->prefix('/film')->group(function() {
    Route::any('/list', 'list');
    Route::any('/detail/{film_id}', 'detail');
    Route::any('/print/{film_id}', 'print')->middleware('can:print film');
});

// Login
Route::get('/login', [LoginController::class, 'login']);
Route::post('/login', [LoginController::class, 'auth']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::get('/', function () {
    return view('welcome');
});
